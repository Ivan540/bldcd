/*
 * A4960.h
 *
 *  Created on: 21 дек. 2018 г.
 *      Author: ivan
 */

#ifndef A4960_H_
#define A4960_H_
///DEFINE REGISTER
unsigned char RUN_H=0b11111010;//10:11 Synchro set, 7:9  Synchro window, 6 stop fault, 4:5 diag set,
unsigned char RUN_L=0b11111010;//3 rst control, 2 brk, 1 dir, 0 run
//CONFIG0
unsigned char CONFIG0_H=0b00010010;//10:11 - comutation blank time (n) 6:9 - blank time (временное отключение защиты по току),
unsigned char CONFIG0_L=0b00010100;//0:5 dead time
//CONFIG1
unsigned char CONFIG1_H=0b00110011;//6:9 Current sense reference //400 mV
unsigned char CONFIG1_L=0b11100000;//0:5 Treshold (n)
//CONFIG2(PWM)
unsigned char CONFIG2_H=0b01010000;//0:4 off-time (n)
unsigned char CONFIG2_L=0b00010000;
//CONFIG3(HOLD) определяет срыв на старте
unsigned char CONFIG3_H=0b01110000;//8 IDS(контроль тока 0, контроль скважности 1) 4:7 стартовый момент
unsigned char CONFIG3_L=0b01010100;//0:3 Время вращения в синхронном режиме
//CONFIG4(Start Com)
unsigned char CONFIG4_H=0b10010000;//4:7 Время конца комутации (n),0:3 время начала комутации (n)
unsigned char CONFIG4_L=0b11110100;
//CONFIG5(Ramp)
unsigned char CONFIG5_H=0b10111111;//8:11 Опережение,  4:7 что-то с опоорной напругой при опережении или в целом,
unsigned char CONFIG5_L=0b10000000;//0:3 что-то непонятное со временем ускорения
//MASK
unsigned char MASK_H=0b11010000; //11-teperature w,  thermal shutdown, Loss sinchro, bootstrap a, bootstrap b, bootstrap c,
unsigned char MASK_L=0b10000000;// a-h, a-l,b-h, b-l, c-h,c-l
//DIAGNOSTIC
unsigned char DIAG_H=0b11000000;//15 General Fault, 14 power reset, undervoltage, temper w, temperr shtd, loss sinch,
unsigned char DIAG_L=0b00000000;//bootstrap a, bootstrap b, bootstrap c, a-h, a-l,b-h, b-l, c-h,c-l

void SPI_WriteArray(uint8_t num, uint8_t *data)
{
	PORTB &= ~(1<<DDB2);
   while(num--){
      SPDR = *data++;
      while(!(SPSR & (1<<SPIF)));
   }
   PORTB |= (1<<DDB2);
}
void config_4960(void)
{

	uint8_t buf1[2]={CONFIG3_H, CONFIG3_L};
	SPI_WriteArray(2, buf1);
	_delay_ms(10);
	uint8_t start[2]={RUN_H, RUN_L};
		SPI_WriteArray(2, start);
		_delay_ms(10);
	uint8_t buf2[2]={CONFIG5_H, CONFIG5_L};
		SPI_WriteArray(2, buf2);
		_delay_ms(10);

}


#endif /* A4960_H_ */
