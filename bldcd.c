/*
 * bldcd.c
 *
 *  Created on: 4 дек. 2018 г.
 *      Author: ivan
 */

#define  F_CPU 8000000L
#include <avr/interrupt.h>
#include <avr/io.h>
#include <avr/iom8.h>
#include <util/delay.h>
#include <inttypes.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <math.h>
#include <avr/eeprom.h>
#include "A4960.h"


//UART SPEED
#define BAUD 9600
#define UBRR_VAL F_CPU/16/BAUD-1

#define LED1 3
#define LED_PORT1 PORTD

#define DATA_REGISTER_EMPTY (1<<UDRE)
#define RX_COMPLETE (1<<RXC)
#define FRAMING_ERROR (1<<FE)
#define PARITY_ERROR (1<<PE)
#define DATA_OVERRUN (1<<DOR)
#define BUFFER_SIZE 2
char buffer[BUFFER_SIZE];





unsigned char rx_wr_index=3;
unsigned char rx_rd_inde;
unsigned char rx_counter;
unsigned char read_enable=0;
unsigned char spd=0;
unsigned char dir=0b00000010;
unsigned char buf_dir_l=1;
unsigned char buf_dir_h=0;
unsigned char buf_dir_r;
unsigned char start_flag=0;


void USART_SendChar(unsigned char sym)
{
	_delay_us(10);
	while(!(UCSRA & (1<<UDRE)))
	_delay_us(10);
	UDR = sym;
}



void UARTSend(long int data) {
	DDRD|=0X02;//TXD
	UDR=0XBB;// SOH
	while(!(UCSRA&(1<<UDRE)));
	_delay_ms(10);
	UDR=data>>8;
	while(!(UCSRA&(1<<UDRE)));
	_delay_ms(10);
	UDR=data;
	DDRD&=0XFD;
	_delay_ms(10);
}

void UARTSend1(long int data) {
	DDRD|=0X02;//TXD
	UDR=data;
	while(!(UCSRA&(1<<UDRE)));

	DDRD&=0XFD;
	_delay_ms(10);
}

ISR(USART_RXC_vect)
{
	//UARTSendChar(0xEE);
	//LED_PORT1=1<<LED1;
	//LED_PORT1=1<<LED1;

		char status,data;
		status=UCSRA;
		data=UDR;

		if ((status & (FRAMING_ERROR | DATA_OVERRUN))==0)
		{
			if ((data == 0xBB)&&(rx_wr_index == 3)&&(read_enable == 0))
						{	//LED_PORT1=0<<LED1;

				rx_wr_index=0;
				read_enable = 1;
						}
				if (read_enable == 1)
							{
								rx_wr_index++;
								if (rx_wr_index == 2)
								{
									buf_dir_h=data;
									if (data == 0xAA)
									{
										if (buf_dir_l==1)
											dir=0b00000011;
										else
										{
											buf_dir_l=1;
											uint8_t buf[2]={0b11111010, 0b00000110};
											SPI_WriteArray(2, buf);
											dir=0b00000011;
											OCR1A=0x10;
										}

									}
									if (data == 0x55)
									{
									if (buf_dir_l==0)
									dir=0b00000001;
									else
									{
										//LED_PORT1=1<<LED1;
									buf_dir_l=0;
									uint8_t buf[2]={0b11111010, 0b00000100};
									SPI_WriteArray(2, buf);
									dir=0b00000001;
									OCR1A=0x10;
																			}
									}

									if (data == 0xCC)
									dir=0b00000110;
									if (data == 0xDD)
									dir=0b00000100;

								}
								if (rx_wr_index == 3)
								{

									if (spd==0)
									{
										OCR1A=0x10;
										start_flag=1;
									}
									spd=data;
								//	LED_PORT1=1<<LED1;
									read_enable = 0;

								}
							}
						}
			}





void spi_init(void)
{
	DDRB |= (1<<DDB2)|(1<<DDB3)|(1<<DDB5)|(0<<DDB4);
	PORTB |= (1<<DDB2)|(1<<DDB3)|(1<<DDB5)|(1<<DDB4);
	SPCR = ( 1 << SPE ) | ( 1 << MSTR )| ( 0 << SPR0 )|(1 << SPR1)|(0 << DORD) ;// Enable SPI, Master, set clock rate fck/16
	SPSR = 0x00;
}

	void transmitMasterSPI(int SPIData )	// Perform a SPI transmission
	{
		int SPIData_h, SPIData_l;
		SPIData_h = SPIData;
		SPIData_l = SPIData>>8;
		PORTB &= ~(1<<DDB2);
		SPDR = SPIData_h;								// Start transmission
		while( !( SPSR & ( 1 << SPIF )));			// Wait for transmission complete

		SPDR = SPIData_l;
		while( !( SPSR & ( 1 << SPIF )));
		SPSR&=0X7F;
		PORTB |= (1<<DDB2);
	}



void pwm_init(void)
{
    DDRB |= (1<<DDB1);
    PORTB|=(1<<PB4);
    OCR1A = 128;  // set PWM for 50% duty cycle
    TCCR1A=(1<<COM1A1)|(1<<WGM10); //На выводе OC1A единица, когда OCR1A==TCNT1, восьмибитный ШИМ
    TCCR1B=(1<<CS10);
}


void usart_init (unsigned int speed)
{
	UBRRH=(unsigned char)(speed>>8);// Baud Rate
	UBRRL=(unsigned char) speed;// Baud Rate
	DDRD|=0X02;//TXD
	UCSRA=0x00;
	UCSRB|=(1<<TXEN)|(1<<RXEN)|(1<<RXCIE)|(0<<TXCIE);// Разрешение приёма и передачи
	UCSRB|=(1<<RXCIE);// Разрешение прерываний по приему
	UCSRC=(1<<UCSZ1)|(1<<UCSZ0)|(1<<URSEL);// Установка формата посылки: 8 бит данных, 1 стоп-бит
}

void init_devices(void)
{
	sei();
	pwm_init();
	spi_init();
	usart_init(UBRR_VAL);
}

void USART_SendStr(char * s)
{
	_delay_ms(1);
	while(*(s)!='\0')
	{
	USART_SendChar(*(s++));
	}
}

void send_as_str(unsigned int data)
{
	char bufer[12];
	sprintf(bufer, "%u",  data );
	USART_SendStr(bufer);
	USART_SendChar(13);
	USART_SendChar(10);
}

void float_as_str(float data)
{
	char bufer[12];
	sprintf(bufer,"%.2f", data);
	USART_SendStr(bufer);
	USART_SendChar(13);
	USART_SendChar(10);
}

int main(void)
{
	init_devices();
	DDRD=0b00001000;
	buffer[0]=0x00;
	buffer[1]=0x00;

				LED_PORT1=1<<LED1;
				config_4960();
				_delay_ms(3000);
				LED_PORT1=0<<LED1;
	//uint8_t buf[2]={0b10111111, 0b10000000};//опережение фазы 4 бита после 4 адреса 7:4 стартовый момент
	//		SPI_WriteArray(2, buf);
	while(1)
	{
		uint8_t buf[2]={RUN_H, dir};
		SPI_WriteArray(2, buf);

		if (start_flag==1){
			OCR1A=0x10;
			LED_PORT1=1<<LED1;
			_delay_ms(10);
			start_flag=0;
			LED_PORT1=0<<LED1;
		}
		else
		{
		OCR1A = spd;
		}
		UARTSend1(spd);

		//LED_PORT1=0<<LED1;
	//	_delay_ms(100);
		//LED_PORT1=1<<LED1;

	}
}

